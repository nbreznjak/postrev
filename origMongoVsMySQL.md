# MongoDB vs. MySQL: the differences explained

MySQL has become an inexpensive option for organizations around the globe that need a relational database. However, as the variety and volume of data have increased in recent years, non-relational databases like MongoDB have arisen to meet the new needs of our fluid data.

## Who Uses These Databases?

**MySQL**: MySQL has generated a strong following since it was started in 1995. Some organizations that use MySQL include GitHub, US Navy, NASA, Tesla, Netflix, WeChat, Facebook, Zendesk, Twitter, Zappos, YouTube, Spotify. You can check the full list here: [https://www.mysql.com/customers/](https://www.mysql.com/customers/).

**MongoDB**: MongoDB was released in 2009 and is used by many organizations including Google, UPS, Facebook, Cisco, eBay, BOSH, Adobe, SAP, Forbes, and many more. You can check the full list here: [https://www.mongodb.com/who-uses-mongodb](https://www.mongodb.com/who-uses-mongodb).

## What About Database Structure?

**MySQL**: MySQL is an open-source relational database management system (RDBMS). Just like all other relational databases, MySQL uses tables, constraints, triggers, roles, stored procedures and views as the core components that you work with.

A table consists of rows, and each row contains a same set of columns. MySQL uses primary keys to uniquely identify each row (a.k.a **record**) in a table, and foreign keys to assure the referential integrity between two related tables.

**MongoDB**: In MongoDB, data is stored in JSON-like documents that can have varied structures. To improve query speed, MongoDB can store related data together, which is accessed using the MongoDB query language. MongoDB is schema-free, allowing you to create documents without having to define the structure of the document first. These documents can be easily changed by adding or deleting fields.

In MongoDB, documents can have their unique structure. New fields can be added at any time and contain any value. This type of functionality would require a relational database to be restructured.

While keys have to be unique for a document, you're allowed to have the same key used in other documents.

Using the MongoDB data model, you can represent hierarchical relationships, data arrays, and other complex structures in the database. In some cases, MongoDB performance is improved over MySQL because MongoDB does not use joins to connect data, improving performance.

## Are Indexes Needed?
Indexes enhance database performance, as they allow the database server to find and retrieve specific rows much faster than without an index. But, indexes add a certain overhead to the database system as a whole, so they should be used sensibly.

Without an index, the database server must begin with the first row and then read through the entire table to find the relevant rows. The larger the table, the more costly operation.

Both MySQL and MongoDB use indexes to allow them to find data quickly.

**MySQL**: Most MySQL indexes (PRIMARY KEY, UNIQUE, INDEX, and FULLTEXT) are stored in B-trees. Exceptions include the indexes on spatial data types that use R-trees. MySQL also supports hash indexes and InnoDB engine uses inverted lists for FULLTEXT indexes.

**MongoDB**: In MongoDB, if an index is not found, every document within a collection must be scanned to select the documents that provide a match to the query statement.

## How Are Their Queries Different?

Selecting records from the customer table:

**MySQL**: `SELECT * FROM customer`

**MongoDB**: `db.customer.find()`

Inserting records into the customer table:

**MySQL**: `INSERT INTO customer (cust_id, branch, status) VALUES ('appl01', 'main', 'A')`

**MongoDB**: `db.customer.insert({ cust_id: 'appl01', branch: 'main', status: 'A' })`

Updating records in the customer table:

**MySQL**:  `UPDATE customer SET branch = 'main' WHERE custage > 2`

**MongoDB**: `db.customer.update( { custage: { $gt: 2 } }, { $set: { branch: 'main' } }, { multi: true } )`

MySQL can be subject to SQL injection attacks, making it vulnerable. Since MongoDB uses object querying, where documents are passed to explain what is being queried, it reduces the risk of attack as MongoDB doesn’t have a language to parse.

## Where (And How) Are These Databases Deployed?

**MySQL**: MySQL is written in C and C++ and has binaries for the following systems: Microsoft Windows, OS X, Linux, AIX, BSDi, FreeBSD, HP-UX, IRIX, NetBSD, Novell Netware, and many more.

To download MySQL go to the [MySQL download](https://www.mysql.com/downloads/) page. There are installation instructions for Microsoft Windows, Linux, or OS X.

**MongoDB**: MongoDB was written in C++, and has support for the following programming languages: Actionscript, C, C#, C++, Clojure, ColdFusion, D, Dart, Delphi, Erlang, Go, Groovy, Haskell, Java, JavaScript, Lisp, Lua, MatLab, Perl, PHP, PowerShell, Prolog, Python, R, Ruby, Scala, Smalltalk.

[Panoply](https://panoply.io/integrations/?_ga=2.176540709.1288302475.1521499656-2117102265.1515398959) has connectors for MySQL and MongoDB providing a no-coding solution that does not require data preparation or transformation. This means you can consolidate data from MySQL, MongoDB, cloud, and more into a single data management platform.

## What Types Of Replication / Clustering Are Available?
**Replication** is a process that enables you to have multiple copies of the data copied automatically from 'master' to 'slave' databases. There are multiple benefits to this, and few of them being:

+ backup
+ spreading the load to improve performance
+ analytics team can work on one of the slave databases, thus not hurting the performance of the main database in case of long-running and intensive queries

**Clustering**, in the context of databases, refers to using shared storage and putting more database front-ends on it. The front end servers share an IP address and cluster network name that clients use to connect, and they decide between themselves who is currently in charge of serving clients requests.

**MySQL**: Replication in MySQL is one-way asynchronous replication where one server acts as a master and others as slaves. You can replicate all databases, selected databases or even selected tables within a database.

MySQL Cluster is a technology providing shared-nothing (no single point of failure) clustering and auto-sharding (partitioning) for the MySQL database management system. 

Internally MySQL Cluster uses synchronous replication through a two-phase commit mechanism to guarantee that data is written to multiple nodes. This is in contrast to what is usually referred to as "MySQL Replication", which is asynchronous.

**MongoDB**: MongoDB supports built-in replication, sharding, and auto-elections. Using auto-elections, you can set up a secondary database to automatically take over if the primary database fails. Sharding allows for horizontal scaling, which is difficult to implement in MySQL.

MongoDB uses replica sets to create multiple copies of the data. Each member of the replica set can have the role of primary or secondary at any point in the process. Reads and writes are done on the primary replica by default and then replicated to the secondary replicas.

## Who's Currently Behind The Databases?

**MySQL**: MySQL was founded by the Finnish/Swedish company MySQL AB, which was started by David Axmark, Allan Larsson, and Michael "Monty" Widenius. In 1995, the first version of MySQL was released. The original version was designed for personal use but later evolved to be an enterprise-grade database. In 2008, Sun Microsystems bought MySQL AB. In 2009, Oracle acquired Sun Microsystems and got MySQL as part of the deal.

MySQL is currently owned by the Oracle Corporation.

**MongoDB**: 10gen started developing MongoDB in 2007 with the idea for the name coming from the word "humongous". It was released in 2009 and has become a popular NoSQL database. 10gen was later renamed MongoDB, Inc. and continues to do development on the software as well as sales for their enterprise solution.

## Who Provides Support?

**MySQL**: MySQL offers technical support services as part of Oracle's lifetime support. The support team contains MySQL developers and support engineers who offer 24/7 support as well as bug fixes, patches, and maintenance releases.

Oracle offers MySQL Premier Support, Extended Support, and Sustaining Support depending upon your needs.

**MongoDB**: MongoDB, Inc offers a support community via the Community Support Forum (https://www.mongodbdb.com/community), ServerFault, and StackOverflow. Users can also get enterprise support 24x7 with optional lifecycle via Enterprise-grade support.

## Who Supplies Ongoing Development?

**MySQL**: Ongoing development is done by the Oracle Corporation, and development decisions are not open to the public. Security releases come out quite often.

**MongoDB**: Ongoing development is done by MongoDB, Inc.

## Who Maintains The Documentation?

**MySQL**: MySQL documentation is maintained by the Oracle Corporation and can be found at https://dev.mysql.com/doc/

**MongoDB**: The MongoDB documentation is maintained by MongoDB, Inc. and can be found at https://docs.MongoDB.com/.

Very useful community sites are the omnipresent [StackOverflow](https://stackoverflow.com/) and a bit more database-specific [Stackexchange for Databases](https://dba.stackexchange.com/).

## Is There An Active Community?

**MySQL**: For MySQL, documentation is maintained by the Oracle Corporation.

**MariaDB**: For MariaDB, the main steward is the MariaDB Foundation, but other people can participate in development and documentation.

# Is There An Active Community?
**MySQL**: MySQL is owned and managed by the Oracle Corporation. Oracle offers a Developer Zone on the MySQL website, which can be found at https://forums.mysql.com/. The site contains a variety of forums for running MySQL.

You can view additional information at:

+ [MySQL Wiki](https://community.oracle.com/community/database/my-sql)
+ [Oracle MySQL Events](http://events.oracle.com/search/search?group=Events&keyword=mysql)
+ [MySQL Events](https://www.mysql.com/news-and-events/events/)
+ [List of MySQL user groups](https://community.oracle.com/docs/DOC-917215)

**MongoDB**: The MongoDB community can be found at https://www.MongoDB.com/community. This page provides a variety of events, webinars, user groups, and MongoDB University.

## Which Database Is Right For Your Business?

When making a choice between MySQL and MongoDB, there are a variety of factors to consider:

**MySQL**: There are many use cases for a relational database like MySQL. Any type of application that requires multi-row transactions such as an accounting system, would be better suited for a relational database. MongoDB is not an easy replacement for legacy systems that were built for relational databases.

**MongoDB**: On the other hand, there are a variety of use cases where MongoDB is well-suited. Some of these include real-time analytics, content management, the internet of things, mobile, and other types of applications that are new and can take advantage of what MongoDB has to offer.

Some scenarios where MongoDB may be an option include:

**No clear schema definition**
There are several instances where MongoDB can be a good choice when dealing with databases with no clear schema definition.

In situations where:

+ During the design phase, you can't define the schema for your database.
+ You find you are de-normalizing a database schema. In MongoDB, documents can be used to store unstructured data and make it easier to update and retrieve.
+ Your database is growing, but your schema is not stable. In MySQL, a table can degrade if it goes over 10 GB, MongoDB does not have this issue.

When adding new columns to a relational database like MySQL, it can lock up the entire database and cause performance issues. With MongoDB, since it is schema-less, you can add new fields, and it won't affect existing rows.

**Write load is high**
For an environment that has many writes compared to reads, MongoDB can be a good choice. MongoDB was written to allow a high insert rate and doesn't have to worry about transaction safety, unlike MySQL.

**No database administrator**
If you don't have a database administrator, MongoDB may be worth considering.

**You lose connectivity to your other databases**
If you have an environment without reliable connectivity to your other servers, the high availability of MongoDB may be helpful.

![MySQL vs MongoDB](https://blog.panoply.io/hs-fs/hubfs/MySQL%20vs%20MongoDB.png?t=1521567637525&width=1500&height=634&name=MySQL%20vs%20MongoDB.png)

MySQL and MongoDB both have their strengths and weaknesses. If your data requires multi-row transactions or you have to support a legacy application, a relational database may be the right choice for your organization. However, if you need a more flexible, schema-free solution that can work with unstructured data, MongoDB is worth considering.

Regardless of which one you select, [Panoply](https://panoply.io/?_ga=2.180259748.1288302475.1521499656-2117102265.1515398959) can create single data management that connects your MySQL, MongoDB, cloud and more with no coding required.